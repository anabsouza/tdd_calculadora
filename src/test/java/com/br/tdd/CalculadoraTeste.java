package com.br.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

public class CalculadoraTeste {

    @Test
    public void testarSomaDeDoisNumero(){
        int resultado = Calculadora.soma(2, 2);
        Assertions.assertEquals(4, resultado);
    }

    @Test
    public void testarDivisaoDoisNumero(){
        int resultado = Calculadora.divisao(6, 2);
        Assertions.assertEquals(3, resultado);
    }

    @Test
    public void testarMultiplicacaoDoisNumero(){
        int resultado = Calculadora.multiplicacao(4, 2);
        Assertions.assertEquals(8, resultado);
    }
    //////////////////////////////////////////////////
    //// Usando NUMEROS FLUTUANTES
    //////////////////////////////////////////////////////////
    @Test
    public void testarSomaDeDoisNumero(){
        BigDecimal resultado = Calculadora.somaflutuante(2.0, 2.2);
        Assertions.assertEquals(4.2, resultado);
    }

    @Test
    public void testarDivisaoDoisNumero(){
        BigDecimal resultado = Calculadora.divisaoflutuante(6.0, 3.0);
        Assertions.assertEquals(18.0, resultado);
    }

    @Test
    public void testarMultiplicacaoDoisNumero(){
        BigDecimal resultado = Calculadora.multiplicacaoflutuante(4.2, 4.5);
        Assertions.assertEquals(18.9, resultado);
    }

}
