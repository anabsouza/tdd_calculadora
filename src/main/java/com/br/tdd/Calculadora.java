package com.br.tdd;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public class Calculadora {

    public static int soma(int primeiroNumero, int segundoNumero) {
        int resultado = primeiroNumero + segundoNumero;
        return resultado;
    }

    public static int divisao(int primeiroNumero, int segundoNumero) {
        int resultado = primeiroNumero / segundoNumero;
        return resultado;
    }

    public static int multiplicacao(int primeiroNumero, int segundoNumero) {
        int resultado = primeiroNumero * segundoNumero;
        return resultado;
    }

    ///// ***** USANDO NUMEROS FLUTUANTES


    public static BigDecimal somaflutuante(double primeiroNumero, double segundoNumero) {
        return BigDecimal.valueOf(primeiroNumero) + BigDecimal.valueOf(segundoNumero);
    }

    public static BigDecimal divisaoflutuante(int primeiroNumero, int segundoNumero) {
        if (BigDecimal.valueOf(segundoNumero).equals(BigDecimal.valueOf(0.0)))
        {
            throw new RuntimeException("Não é possível divisão por ZERO");
            }
            return new BigDecimal(primeiroNumero, new MathContext(3, RoundingMode.HALF_EVEN))
            .divide(new BigDecimal(segundoNumero, new MathContext(3, RoundingMode.HALF_EVEN)),
                    new MathContext(3, RoundingMode.HALF_EVEN));
    }

    public static BigDecimal multiplicacaoflutuante(int primeiroNumero, int segundoNumero) {
        return BigDecimal.valueOf(primeiroNumero).multiply(BigDecimal.valueOf(segundoNumero));
    }
}

